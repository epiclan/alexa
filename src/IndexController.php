<?php
	namespace Epic\Alexa;
	use Alexa\Request\Request;
	use Alexa\Response\Response;

	class IndexController {
		public function handleRequest() {
			$url = $this->getURL();
			try {
				switch ($url) {
					case '/ping':
						$this->ping();
						break;
					default:
						$this->defaultHandler();
						break;
				}
			} catch (\Exception $e) {
				header('Status: 500 Server Error');
				$response = (object) [
					'code' => $e->getCode(),
					'message' => $e->getMessage()
				];
				$this->render($response);
			}
		}

		protected function defaultHandler() {
			$request = Request::fromData($this->getBody());
			switch ($request->intentName) {
				case 'GetEventDetails':
					$this->getEventDetails($request);
					break;
				case 'GetEventLocation':
					$this->getEventLocation($request);
					break;
				case 'GetEventStats':
					$this->getEventStats($request);
					break;
				case 'AMAZON.HelpIntent':
					$this->help($request);
					break;
				default:
					$this->noIntentFound($request);
					break;
			} 
		}

		protected function help($request) {
                        $response = new Response;
                        $response->respond("I can provide you with information about epic.LAN events. Try asking me about the next event.");
                        $this->render($response->render());

		}

		protected function lookupEvent($name) {
			if (strpos($name, 'south') !== false) {
				$event = 'elite-south-6';
			} elseif (strpos($name, 'lite') !== false) {
				$event = 'elite18';
			} else {
				$event = 'epic19';
			}
			$result = json_decode(file_get_contents("https://www.epiclan.co.uk/{$event}/stats?json=1"));
			$result->alexaName = str_replace('epic.', '<phoneme alphabet="ipa" ph="/ˈɛpɪk/ ">epic</phoneme> ', $result->name);
			return $result;
		}

		protected function getEventDetails($request) {
			$event = $this->lookupEvent($request->slots['event']);
			$response = new Response;
			$spaces = $event->capacity - $event->paid_participants;
			$date = '<say-as interpret-as="date">' . date('????md', $event->start) . '</say-as>'	;
			$response->respond("{$event->alexaName} is taking place on {$date} at {$event->location}. There are {$spaces} places remaining.");
			$response->endSession();
			$this->render($response->render());
		}


                protected function getEventLocation($request) {
                        $event = $this->lookupEvent($request->slots['event']);
                        $response = new Response;
                        $response->respond("{$event->alexaName} is taking place at {$event->location}");
			$response->endSession();
                        $this->render($response->render());
                }


                protected function getEventStats($request) {
                        $event = $this->lookupEvent($request->slots['event']);
                        $response = new Response;
                        $spaces = $event->capacity - $event->paid_participants;
                        $response->respond("There are {$event->paid_participants} people paid for {$event->alexaName}. There are {$spaces} places remaining.");
			$response->endSession();
                        $this->render($response->render());
                }

		protected function noIntentFound($request) {
			$response = new Response;
			$response->respond("I'm sorry, I don't know how to do that.");
			$this->render($response->render());
		}

		protected function ping() {
			$result = (object) [
				'timestamp' => date('r'),
				'url' => $this->getURL()
			];
			$body = $this->getBody();
			if ($body) {
				$result->body = $body;
			}
			$this->render($result);
		}

		protected function getBody() {
			$body = file_get_contents('php://input');
			return json_decode($body, true);
		}

		protected function getURL() {
			if (!isset($_SERVER['REQUEST_URI'])) {
				return '';
			}
			return $_SERVER['REQUEST_URI'];
		}

		protected function render($output = null, $encoded = false) {
			header('Content-Type: application/json');
			echo json_encode($output);
			exit(0);
		}
	}
