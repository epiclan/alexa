<?php
	require_once('../vendor/autoload.php');

	use Epic\Alexa\IndexController;

	$controller = new IndexController;
	$controller->handleRequest();
